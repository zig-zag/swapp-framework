#!/bin/bash
# Build Application

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR/..

echo Given Environment Variables:
env

echo Used working directory
pwd

echo Composer install run...
composer install --no-interaction

echo Run Unit Tests
$DIR/phpunit -c $DIR/../phpunit.xml
