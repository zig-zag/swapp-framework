<?php
namespace App\Tests\Model;

class HelperTest extends \PHPUnit_Framework_TestCase
{
    public function testSum()
    {
        $helper = new \App\Model\Helper();
        $a = 1;
        $b = 2;

        // Assert
        $this->assertEquals($a + $b, $helper->sum($a, $b));
    }
}
