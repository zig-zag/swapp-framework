#!/bin/bash
# Init Foreman

set -e 

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

puppet --version || (\
    sh -c 'echo "deb http://deb.theforeman.org/ trusty 1.5" > /etc/apt/sources.list.d/foreman.list' && \
    sh -c 'echo "deb http://deb.theforeman.org/ plugins 1.5" >> /etc/apt/sources.list.d/foreman.list' && \
    wget -q http://deb.theforeman.org/pubkey.gpg -O- | sudo apt-key add - && \
    apt-get update && \
    apt-get install -y foreman-installer && \
    foreman-installer && \
    puppet agent --test && \
    puppet module install -i /etc/puppet/environments/production/modules saz/ntp && \
    puppet module install -i /etc/puppet/environments/production/modules puppetlabs-stdlib \
)