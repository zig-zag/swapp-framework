package { 'git':
    ensure => present
}

$packages = hiera('extra_packages')

notify { 'extra_pkg':
    message => 'successfully setup extra packages'
}

package { $packages:
    before => Notify['extra_pkg'],
    ensure => present
}

host { 'foreman_host':
  name         => hiera('hostname'),
  ensure       => present,
  comment      => 'Foreman Host FQDN',
  host_aliases => hiera('hostname_aliases'),
}

file_line { '/etc/puppet/puppet.conf':
  path => '/etc/puppet/puppet.conf',
  line => '%show_diff     = true',
  # match  => 'show_diff',
  match  => 'show_diff\ *=\ *false',
}