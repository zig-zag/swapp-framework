#!/bin/bash
# Install Puppet Agent

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

puppet --version || (\
    cd /tmp && 
    curl https://apt.puppetlabs.com/puppetlabs-release-trusty.deb -o /tmp/puppetlabs-release-trusty.deb && \
    dpkg -i puppetlabs-release-trusty.deb && \
    apt-get update && \
    apt-get install -y puppet && \
    rm puppetlabs-release-trusty.deb &&
    puppet --version \
)

